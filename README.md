# Precaristas subtitles translation

Simple, yet functional, repository for improving and increasing the subtitles of the "Precaristas" documentary.  
Link to the video: https://kolektiva.media/videos/watch/21e32246-2afa-42d9-a70a-c5d16c7cc9ba

## Submissions

Either open a PR/MR or send your changes via mail to aga@agamsterdam.org